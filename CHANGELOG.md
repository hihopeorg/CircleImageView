## 1.0.3

- 适配DevEco Studio 3.1Beta1及以上版本。
- 适配OpenHarmony SDK API version 9及以上版本。

## v1.0.2

- api8升级到api9

## v1.0.0

- 详细功能：
  1. 支持图片中心裁剪的缩放方式
  2. 支持圆形边框设置
